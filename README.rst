FZFSPell: Vim spelling suggestions with fzf
-------------------------------------------

This is completely shamelessly stolen code from the blogpost by 
Corey Alexander “`VIM Spelling Suggestions with fzf`_”.

Replaces ``z=`` with a version that filters all suggested 
spelling options with fzf managed list.

.. _`VIM Spelling Suggestions with fzf`:
   https://coreyja.com/vim-spelling-suggestions-fzf/
