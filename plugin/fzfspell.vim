function! FzfSpellSink(word)
  exe 'normal! "_ciw'.a:word
endfunction
function! FzfSpell()
  let suggestions = spellsuggest(expand("<cword>"))
  return fzf#run(fzf#wrap({'source': suggestions,
              \ 'sink': function("FzfSpellSink")}))
endfunction
nnoremap z= :call FzfSpell()<CR>
